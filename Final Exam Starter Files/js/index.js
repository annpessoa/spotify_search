// TASK 1: Obtain your own OAuth Token and replace this one -- this "Token" 
//         will be expired by exam time and will NOT work -- just a sample!
//         (Note: Keep refreshing your Token about every 30 mins)

// NOTE: I have added functions to this script to update the oAuthToken from the 
//       input box in index.html. Simply enter in the new token in the text box 
//       and the token will be updated
var default_OAuthToken = "BQB5xNePGG8Ec-82HseXodMK4gPERvHUmTf84Wt9PR65hHuwy5kghf3_zKi4uvxQq9pzFKW3ytUhRyT6KH91frrm_RW6uRzzjTOFYKRr8Pi1ZJipdDoRT3_7XZPHC3NN2gCzsqKC87YTHh4-";

var oAuthToken = "BQB5xNePGG8Ec-82HseXodMK4gPERvHUmTf84Wt9PR65hHuwy5kghf3_zKi4uvxQq9pzFKW3ytUhRyT6KH91frrm_RW6uRzzjTOFYKRr8Pi1ZJipdDoRT3_7XZPHC3NN2gCzsqKC87YTHh4-";

$("#auth-token").val(oAuthToken);

//base URL
var baseURL = "https://api.spotify.com/v1/search";
var endpoint = "&type=artist";

//artist name
var artist = "?q=";

var myURL = "";
// TASK 2: Complete and test this function that inserts an artist name into search url.  
//         Use a JavaScript REGEX search and replace the {artist_name} template in the 
//         above search_base_url use this function to concantenate (build) your search url
function getSearchUrl(name) {
    var updatedName = name.split(' ').join('+');
    artist += updatedName;
    myURL = baseURL + artist + endpoint;
}

// TASK 3: After verifying that the correct search URL is present 
//         after the api_search() is called, use JQuery $(select).manipulate
//         to store this url in the input field selected by "input#search-url" 
function updateSearchUrl(){
    $('input#search-url').val(myURL);
}

$("#send-req").on('click', function (e) {
    var artistName = $('#artist-name').val();
    getSearchUrl(artistName);
    updateSearchUrl();
    console.log("request sent");
    $.ajax({
        method: "GET",
        url: myURL,
        dataType: "json",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + oAuthToken
        },
        success: function (data) {
            // TASK 4: - Add code to convert the raw data object into a
            //           JSON string before storing to the element identified by
            //           "textarea#spotify-data".  (This will be a useful string to
            //           copy-paste into an editor like VS Code to examine JSON data)
            
            var myJSON = JSON.stringify(data);
            $('textarea#spotify-data').val(myJSON);
            
            // TASK 5: Add code to update the HTML field for the artist ID
            $("#artist-id").val(data['artists']['items'][0].id);
            
            // TASK 6: Add code to update the HTML field for the artist Name
            $("#out-name").html(data['artists']['items'][0].name);
            
            // TASK 7: Add code to update the HTML field for the artist Genres
            $("#genre").html(data['artists']['items'][0]['genres'].join(',  '));
            
            // TASK 8: Add code to update the HTML field for the artist URL and Image
            //         Use the images given by index 2 to produce an image that fits OK
            //         on this page.
            $('#artist-url').html(data['artists']['items'][0]['external_urls'].spotify);
            
            $(".col > img").attr("src",data['artists']['items'][0]['images'][0].url);
            $(".col > img").css("width", "160px");
            
            //adding followers
            $("#followers").html(data['artists']['items'][0]['followers'].total);
        },
        error: function (e) {
            let err = JSON.stringify(e);
            console.log(err);
        },
        cache: false
    });
});


$('#auth-token').bind("enterKey",function(e){
   //do stuff here
    oAuthToken = $("#auth-token").val();
    console.log("changed oAuth= " + oAuthToken); 
});

$('#auth-token').keyup(function(e){
    if(e.keyCode == 13)
    {
        $(this).trigger("enterKey");
    }
});
